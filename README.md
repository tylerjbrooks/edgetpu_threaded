## Edge TPU Threaded Example

This application (edgetpu_threaded) is an example of using multiple threads with the 
[Edge TPU](https://coral.ai/products/accelerator/) on a 
i[Raspberry Pi 3 B+](https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/).  One thread
is used to read an image from a file and the other is used to run inference on the image.  It only 
does this once and then quits.  This application is written in C++. 

Many thanks to the [Google Coral](https://coral.ai/) project for providing the TPU accelerator.

Many thanks to the [Tensorflow](https://www.tensorflow.org) project for doing the object detection heavy lifting.

### Installation

Edgetpu_threaded is cross compiled on a desktop and copied to the target rpi3b+.  As such, the build 
process will require some environment variables so it can find the various pieces of 
Detector.  The following is how my environment is setup:
```
# project directory
export RASPBIAN=~/your/workspace/raspbian
# project cross-compiler
export RASPBIANCROSS=arm-linux-gnueabihf-
# TensorFlow location
export TFLOWSDK=$RASPBIAN/tensorflow
# Edgetpu location
export EDGETPUSDK=$RASPBIAN/edgetpu
```

Get Tensorflow, Edgetpu and Edgetpu_threaded like this:
```
cd your/workspace/raspbian
git clone https://gitlab.com:tylerjbrooks/tensorflow.git
cd tensorflow
git checkout d855adf  #tensorflow commit d855adf
cd ..
git clone https://github.com/google-coral/edgetpu
git clone https://gitlab.com:tylerjbrooks/edgetpu_threaded.git

```
note:  the Edgetpu sdk currently only works with the 'd855adf' commit of tensorflow.

Get the Raspbian tool chain like this:
```
sudo apt-get install build-essential
sudo apt-get install g++-arm-linux-gnueabihf
```

At this point, you should have all the software you need to build Detector.

### Build Notes

Start by building Tensorflow Lite:
```
cd your/workspace/raspbian
cd tensorflow/tensorflow/lite/tools/make
./download_dependencies.sh
./build_rpi_lib.sh
```

Build Edgetpu_threaded:
```
cd your/workspace/raspbian
cd edgetpu_threaded
make
```

### Usage
```
edgetpu_threaded <edgetpu model path> <input image path>
  default model: ./models/mobilnet_ssd_v2_coco_quant_postprocess_edgetpu.tflite
  default image: ./images/me_640x480.rgb24
```

The default output looks like this:
```
./edgetpu_threaded 
  buildInterpreter: build model
  buildInterpreter: build model interpreter
  buildInterpreter: find model input dimensions
  buildInterpreter: build resize interpreter
finder: waiting...
finder: resize
finder: invoke
finder:  results
t:0,l:3,b:479,r:632, scor:0.910156, class:14
```
The last line shows the bounding box (top, left, bottom, right) and model 
score and class.

### Discussion
I make this program to explore a problem I was having with the Edge TPU
in a multithreaded environment.

### Notes


### To Do

