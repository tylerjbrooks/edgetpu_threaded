// Example to run a model using one Edge TPU.
// It depends only on tflite and edgetpu.h

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>

#include "edgetpu.h"
#include <tensorflow/lite/builtin_op_data.h>
#include <tensorflow/lite/kernels/register.h>
#include <tensorflow/lite/kernels/internal/tensor_ctypes.h>
#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/model.h"


class Application {

  std::mutex mtx_;
  std::condition_variable cv_;
  bool loaded_;

  std::string model_path_;
  std::string image_path_;

  int image_height_;
  int image_width_;
  int image_channels_;
  int image_size_;
  std::vector<uint8_t> image_;

  std::shared_ptr<edgetpu::EdgeTpuContext> edgetpu_context_;
  std::unique_ptr<tflite::FlatBufferModel> model_;
  std::unique_ptr<tflite::Interpreter> model_interpreter_;
  std::unique_ptr<tflite::Interpreter> resize_interpreter_;

  int model_height_;
  int model_width_;
  int model_channels_;
  int model_size_;

  public:
    Application(const std::string& mp, const std::string& ip) 
      : model_path_(mp), image_path_(mp) {
      loaded_ = false;
    }

    void readImage() {
      // read image (assumed to be 640x480 rgb24)
      image_height_   = 480;
      image_width_    = 640;
      image_channels_ = 3;
      image_size_     = image_height_ * image_width_ * image_channels_;
      image_.reserve(image_size_);
      std::ifstream file(image_path_, std::ios::binary);
      image_.insert(image_.begin(),
          std::istream_iterator<uint8_t>(file),
          std::istream_iterator<uint8_t>());
    }

    void buildInterpreters() {
      std::cout << "  buildInterpreter: ";
      std::cout << "build model" << std::endl;
      model_ = tflite::FlatBufferModel::BuildFromFile(model_path_.c_str());

      // build model interpreter
      std::cout << "  buildInterpreter: ";
      std::cout << "build model interpreter" << std::endl;
      edgetpu_context_ = edgetpu::EdgeTpuManager::GetSingleton()->OpenDevice();
      tflite::ops::builtin::BuiltinOpResolver resolver;
      resolver.AddCustom(edgetpu::kCustomOp, edgetpu::RegisterCustomOp());
      if (tflite::InterpreterBuilder(*model_, resolver)(&model_interpreter_) != kTfLiteOk) {
        std::cerr << "Failed to build model_interpreter_." << std::endl;
      }
      model_interpreter_->SetExternalContext(kTfLiteEdgeTpuContext, edgetpu_context_.get());
      model_interpreter_->SetNumThreads(1);
      if (model_interpreter_->AllocateTensors() != kTfLiteOk) {
        std::cerr << "Failed to allocate tensors." << std::endl;
      }

      // find model input dimensions
      std::cout << "  buildInterpreter: ";
      std::cout << "find model input dimensions" << std::endl;
      int input = model_interpreter_->inputs()[0];
      const std::vector<int> inputs = model_interpreter_->inputs();
      const std::vector<int> outputs = model_interpreter_->outputs();
      TfLiteIntArray* dims = model_interpreter_->tensor(input)->dims;
      model_height_ = dims->data[1];
      model_width_ = dims->data[2];
      model_channels_ = dims->data[3];
      model_size_ = model_height_ * model_width_ * model_channels_;

      // build resize model_interpreter_
      std::cout << "  buildInterpreter: ";
      std::cout << "build resize interpreter" << std::endl;
      resize_interpreter_ = std::make_unique<tflite::Interpreter>();
      int base_index = 0;
      resize_interpreter_->AddTensors(2, &base_index); // two inputs: input and new_size
      resize_interpreter_->AddTensors(1, &base_index); // one output
      resize_interpreter_->SetInputs({0, 1});
      resize_interpreter_->SetOutputs({2});
      TfLiteQuantizationParams quant;
      resize_interpreter_->SetTensorParametersReadWrite(
          0, kTfLiteFloat32, "input",
          {1, image_height_, image_width_, image_channels_}, 
          quant);
      resize_interpreter_->SetTensorParametersReadWrite(
          1, kTfLiteInt32, "new_size", 
          {2}, 
          quant);
      resize_interpreter_->SetTensorParametersReadWrite(
          2, kTfLiteFloat32, "output",
          {1, model_height_, model_width_, model_channels_}, 
          quant);
      tflite::ops::builtin::BuiltinOpResolver resize_resolver;
      const TfLiteRegistration* resize_op =
          resize_resolver.FindOp(tflite::BuiltinOperator_RESIZE_BILINEAR, 1);
      auto* params = reinterpret_cast<TfLiteResizeBilinearParams*>(
          malloc(sizeof(TfLiteResizeBilinearParams)));
      params->align_corners = false;
      resize_interpreter_->AddNodeWithParameters(
          {0, 1}, {2}, nullptr, 0, params, resize_op, nullptr);
      resize_interpreter_->SetExternalContext(kTfLiteEdgeTpuContext, edgetpu_context_.get());
      resize_interpreter_->SetNumThreads(1);
      if (resize_interpreter_->AllocateTensors() != kTfLiteOk) {
        std::cerr << "Failed to allocate resize tensors." << std::endl;
      }
    }

    void resize(std::unique_ptr<tflite::Interpreter>& interpreter,
        uint8_t* out, uint8_t* in,
        int image_height, int image_width, int image_channels, 
        int wanted_height, int wanted_width, int wanted_channels) {

      int number_of_pixels = image_height * image_width * image_channels;

      // fill input image
      // in[] are integers, cannot do memcpy() directly
      auto input = interpreter->typed_tensor<float>(0);
      for (int i = 0; i < number_of_pixels; i++) {
        input[i] = in[i];
      }

      // fill new_sizes
      interpreter->typed_tensor<int>(1)[0] = wanted_height;
      interpreter->typed_tensor<int>(1)[1] = wanted_width;

      interpreter->Invoke();

      auto output = interpreter->typed_tensor<float>(2);
      auto output_number_of_pixels = wanted_height * wanted_width * wanted_channels;

      for (int i = 0; i < output_number_of_pixels; i++) {
        out[i] = (uint8_t)output[i];
      }
    }

    void reader() {
      readImage();

      std::this_thread::sleep_for(std::chrono::milliseconds(2000));
      std::lock_guard<std::mutex> guard(mtx_);
      loaded_ = true;
      cv_.notify_one();
    }

    void finder() {
      buildInterpreters();

      std::cout << "finder: ";
      std::cout << "waiting..." << std::endl;
      std::unique_lock<std::mutex> lck(mtx_);
      cv_.wait(lck, [this] { return loaded_; });

      // resize
      std::cout << "finder: ";
      std::cout << "resize" << std::endl;
      int input = model_interpreter_->inputs()[0];
      if (model_interpreter_->tensor(input)->type == kTfLiteUInt8) {
        resize(resize_interpreter_,
            model_interpreter_->typed_tensor<uint8_t>(input), image_.data(), 
            image_height_, image_width_, image_channels_,
            model_height_, model_width_, model_channels_);
      } else {
        std::cerr << "unrecognized input type" << std::endl;
      }

      // invoke
      std::cout << "finder: ";
      std::cout << "invoke" << std::endl;
      model_interpreter_->Invoke();

      // results
      std::cout << "finder: ";
      std::cout << " results" << std::endl;
      const std::vector<int>& res = model_interpreter_->outputs();
      float* locs = tflite::GetTensorData<float>(model_interpreter_->tensor(res[0]));
      float* clas = tflite::GetTensorData<float>(model_interpreter_->tensor(res[1]));
      float* scor = tflite::GetTensorData<float>(model_interpreter_->tensor(res[2]));
      float* tot  = tflite::GetTensorData<float>(model_interpreter_->tensor(res[3]));
      for (int i = 0; i < (int)tot[0]; i++, locs += 4) {
        if (scor[i] > 0.5f && scor[i] < 1.0f) {

          // clamp
          float top    = fmin(fmax(locs[0], 0.f), 1.f);
          float left   = fmin(fmax(locs[1], 0.f), 1.f);
          float bottom = fmin(fmax(locs[2], 0.f), 1.f);
          float right  = fmin(fmax(locs[3], 0.f), 1.f);
          if (top < bottom) {
            if (left < right) {
              printf("t:%d,l:%d,b:%d,r:%d, scor:%f, class:%d\n",
                  (int)(top   *image_height_), 
                  (int)(left  *image_width_), 
                  (int)(bottom*image_height_), 
                  (int)(right *image_width_), 
                  scor[i], (int)clas[i]);
            }
          }
        }
      }
    }
};

int main(int argc, char* argv[]) {

  // usage
  if (argc != 1 && argc != 3) {
    std::cout << " edgetpu_threaded <edgetpu model> <input image>" << std::endl;
    return 1;
  }

  // requires tpu
  const auto& available_tpus =
      edgetpu::EdgeTpuManager::GetSingleton()->EnumerateEdgeTpu();
  if (available_tpus.size() < 1) {
    std::cerr << "This example requires one Edge TPUs to run." << std::endl;
    return 0;
  }

  // model and image_ paths
  const std::string model_path =
      argc == 3 ? argv[1] : "./models/mobilenet_ssd_v2_coco_quant_postprocess_edgetpu.tflite";
  const std::string image_path =
      argc == 3 ? argv[2] : "./images/me_640x480.rgb24";

  // main application
  Application app (model_path, image_path);
  std::thread finder(&Application::finder, &app);
  std::thread reader(&Application::reader, &app);
  reader.join();
  finder.join();
  return 0;
}
